====
Setup your environment
====


Summary
~~~~

#. `Download and install Virtualbox`_
#. `Download and install Vagrant`_


Download and install VirtualBox
----

During the tutorial we will use Virtual Machines running on Virtual Box.

VirtualBox can be downloaded and then installed from `here <https://www.virtualbox.org/wiki/Downloads>`_.



Download and install Vagrant
----

Vagrant allows the easy configuration and deployment of a VirtualBox environment. For this reason, Vagrant will be used to configure and deploy the Virtual Machine used in the tutorial.

Vagrant can be downloaded and then installed from `here <http://downloads.vagrantup.com/>`_.

For Vagrant to work properly you need to have the latest *VirtualBox Guest Additions* . You can keep VBox Guest Additions up-to-date running the follow::

  shell> vagrant gem install vagrant-vbguest

Load the base vagrant box::

  shell> vagrant box add ndbcluster /path/to/external/usb/lucid32.box

Copy the Vagrantfile in your working directory::
  
  shell> cp /path/to/external/usb/Vagrantfile .

Start the VMs::
  
  shell> vagrant up

If Vagrant will update the VirtualBox Guest Additions, will show a message like the follow::
  
  Removing existing VirtualBox DKMS kernel modules ...done.
  Removing existing VirtualBox non-DKMS kernel modules ...done.
  Building the VirtualBox Guest Additions kernel modules ...done.
  Doing non-kernel setup of the Guest Additions ...done.
  You should restart your guest to make sure the new modules are actually used

Just do a restart::

  shell> for i in 1 2 3 ; do vagrant halt node$i ; done
  shell> vagrant up
